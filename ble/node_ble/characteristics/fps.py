import time 
from pyfingerprint.pyfingerprint import PyFingerprint
import RPi.GPIO as GPIO
GPIO.setwarnings(False)

SUCCESS = 1
FAILURE = 2
GPIO_STAT_LED = 24
#waits for 120 seconds (2 minutes for the person's fingerprint)
WAIT_THRESH = 120 

def openFPS():
    try:
        f = PyFingerprint('/dev/ttyS0', 57600, 0xFFFFFFFF, 0)
        if(f.verifyPassword() == False):
            raise ValueError('The given fingerprint sensor password is wrong!')
        return f
    except Exception as e:
        print 'The fingerprint sensor could not be initialized'
        print 'Exception message: ' + str(e)
        exit(1)


def setupGPIO(pinNum):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pinNum, GPIO.OUT, initial=GPIO.LOW)

#assumes that the associated pinNum is connected to some LED and has been 
#setup by a call to setupGPIO. Blinks that LED for a number of times
def blinkLED(pinNum, numTimes):
    for i in xrange(numTimes):
        GPIO.output(pinNum, True)
        time.sleep(0.5)
        GPIO.output (pinNum, False)
        time.sleep(0.5)

#blinks an external LED based on the result of the operation. If the LED 
#blinks once ==> success
#blinks twice ==> failure
def registerNewFingerprint():
    f = openFPS()
    #check how much storage is left
    setupGPIO(GPIO_STAT_LED)
    num_templates = f.getTemplateCount()
    max_templates = f.getStorageCapacity()
    if(num_templates == max_templates):
        print "Fingerprint sensor at capacity. Please consider deleting templates"
    try:
        print 'Waiting for finger'
        #wait for a finger to appear 
        while(f.readImage() == False):
            pass
        #convert into characteristics for comparison. stores it in char buffer 1
        f.convertImage(1)
        result = f.searchTemplate()
        positionNumber = result[0]
        if(positionNumber >= 0):
            print 'Template already exists at position: ' + str(positionNumber)
            blinkLED(GPIO_STAT_LED, FAILURE)
            exit(0)
        print 'Remove finger'
        time.sleep(2)
        print 'Please press the same finger again'
        while(f.readImage() == False):
            pass
        #convert into characteristics for comparison. stores it in char buffer 2
        f.convertImage(2)
        #make sure ts the same finger
        if(f.compareCharacteristics() == 0):
            blinkLED(GPIO_STAT_LED, FAILURE)
            exit(0)
        #create new template
        f.createTemplate()
        positionID = f.storeTemplate()
        print 'Finger enrolled successfully with ID: ' + str(positionID)
        print '\n'
        blinkLED(GPIO_STAT_LED, SUCCESS)
    except Exception as e: 
        blinkLED(GPIO_STAT_LED, FAILURE)
        print 'Enrollment failed.'
        print 'Exception message: ' + str(e)
        exit(1)

def verifyFingerprint():
    f = openFPS()
    setupGPIO(GPIO_STAT_LED)
    timeout = False
    try: 
        print 'Waiting for finger'
        #gives time in seconds
        init_time = time.time()

        while(f.readImage() == False):
            if(time.time() - init_time >= WAIT_THRESH):
                timeout = True
                break
        if(timeout):
            blinkLED(GPIO_STAT_LED, FAILURE)
            print 'Timed out during verification'
            return False
        f.convertImage(1)
        #search
        result = f.searchTemplate()
        positionNumber = result[0]
        accuracyScore = result[1]
        if(positionNumber == -1):
            blinkLED(GPIO_STAT_LED, FAILURE)
            print 'No match found'
            return False 
        else:
            blinkLED(GPIO_STAT_LED, SUCCESS)
            print 'Successfully identified finger associated with template '  + str(positionNumber) + '\n'
            return True
    except Exception as e:
        blinkLED(GPIO_STAT_LED, FAILURE)
        print 'Operation failed'
        print 'Exception message: ' + str(e)
        exit(1)
       
def deleteFingerprint(templateNum):
    f = openFPS()
    setupGPIO(GPIO_STAT_LED)
    currCount = f.getTemplateCount()
    if(currCount == 0):
        blinkLED(GPIO_STAT_LED, FAILURE)
        print 'No templates to delete'
        return
    print 'Current amount of used templates: ' + str(currCount)
    try: 
        if(f.deleteTemplate(templateNum) == True):
            blinkLED(GPIO_STAT_LED, SUCCESS)
            print 'Template deleted'
            print 'New amount of used templates: ' + str (f.getTemplateCount())
            print '\n'
    except Exception as e:
        blinkLED(GPIO_STAT_LED, FAILURE)
        print'Operation failed'
        print 'Exception message: ' + str(e)
        exit(1)

#depricated
'''
def demo_main():
    while(True):
        is_valid_cmd = False
        while(not(is_valid_cmd)):
            print 'Options \n1==> Register a new fingerprint\n2==>Verify fingerprint\n3==>delete fingerprint\n'
            cmd = input('Please enter a command: ')
            try: 
                cmd = int(cmd)
            except:
                print 'Please enter a number for a command'
            if(1 <= cmd and cmd <= 3):
                is_valid_cmd = True
            else:
                 print 'Please enter a valid command'
        if(cmd == 1):
            registerNewFingerprint()
        elif(cmd == 2):
            verifyFingerprint()
        else:
            deleteFingerprint()
#demo_main()      '''      
