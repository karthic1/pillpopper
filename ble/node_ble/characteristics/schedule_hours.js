var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var PythonShell = require('python-shell');
var exec = require('child_process').exec;
fs = require('fs');

var LOCKED = 1;
var UNLOCKED = 0;
var TIMEOUT_LOCK = 5; // in seconds
var SNO_DELIM = ':';
var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'
var SETUP_CRON_SCRIPT_NAME = 'setup_cron_demo.py'


var BlenoCharacteristic = bleno.Characteristic;

var ScheduleHoursCharacteristic = function() {

 ScheduleHoursCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb04',
    properties: ['read', 'write'],
  });

 this._value = new Number(LOCKED);
};

ScheduleHoursCharacteristic.prototype.onReadRequest = function(offset, callback) {

  console.log('ScheduleHoursCharacteristic - onReadRequest');

  var sno = process.env.SERIAL_NO;
  // this is a string, and then I want to send this back to the user
  // now read the thing, and add the colon
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));

  var returnString = sno + SNO_DELIM + configInfo.hours.toString();

  console.log('ScheduleHoursCharacteristic ' + Buffer.from(returnString, 'utf8'));

  
  // This is returning the SNO:DAY_FREQ_INT
  callback(this.RESULT_SUCCESS, Buffer.from(returnString, 'utf8'));
};

ScheduleHoursCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  console.log('ScheduleHoursCharacteristic - onWriteRequest: value = ' + data);
  var hours = JSON.parse("[" + data + "]");
  console.log('ScheduleHoursCharacteristic - onWriteRequest: Sorted value = ' + hours);
  // write this to the file
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));
  configInfo.hours = hours;

  var writeback = JSON.stringify(configInfo, null, '\t');

  fs.writeFile(CONFIG_JSON_PATH, writeback, 
    function(err) {
      if(err) return console.error(err);
      console.log('ScheduleHoursCharacteristic - done writing to config file!!!');
    })

  var options = 
    { mode: 'text', 
      pythonPath:'/usr/bin/python', 
      pythonOptions:[], 
      scriptPath:'/home/pi/config/', 
      args:[]};

  PythonShell.run(SETUP_CRON_SCRIPT_NAME, options, 
    function(err, results) {
      if(err) return console.error(err);
      console.log("ScheduleHoursCharacteristic - Warning related error from at command abou /bin/sh");
    });

  callback(this.RESULT_SUCCESS, 'DONE');
  
};

util.inherits(ScheduleHoursCharacteristic, BlenoCharacteristic);
module.exports = ScheduleHoursCharacteristic;
