#!usr/bin/bash
#setup code
gpio -g mode 18 pwm
gpio pwm-ms
gpio pwmc 192
gpio pwmr 2000
#make motor go from -180 to 180 infinitely
while true ; do

  gpio -g pwm 18 75
  sleep 1
  gpio -g pwm 18 325
  sleep 1
done
