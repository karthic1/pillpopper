//
//  HourlyScheduleTableViewController.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import CoreBluetooth

protocol HourlyScheduleDelegate {
    func addHourlySchedule(hourString: String)
}

class HourlyScheduleTableViewController: UITableViewController, HourlyScheduleDelegate {
    
    var peripheral:CBPeripheral! = nil
    var characteristic:CBCharacteristic! = nil
    var hours:[String] = []
    let kMaxRowsPerScreen = 15

    override func viewDidLoad() {
        super.viewDidLoad()
        assert(peripheral != nil)
        assert(characteristic != nil)
        self.tableView.allowsSelection = false
        self.tableView.allowsMultipleSelectionDuringEditing = false
        self.tableView.backgroundColor = UIColor.hexStr("20AEFF")


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func getMilitaryTime(hour: String) -> Int {
        let components = hour.components(separatedBy: CharacterSet.init(charactersIn: ": "))
        let hourTime = Int(components[0])!
        let amPm = components[2]
        var militaryTime = 0
        if(amPm[0] == "A") {
            if(hourTime == 12) {
                militaryTime = 0
            } else {
                militaryTime = hourTime
            }
        } else if(amPm[0] == "P") {
            if(hourTime == 12) {
                militaryTime = 12
            } else {
                militaryTime = hourTime + 12
            }
        }
        assert(militaryTime < 24)
        return militaryTime
    }
    
    func sortHours() {
        self.hours = self.hours.sorted(by: { (hour1, hour2) -> Bool in
            let militaryTime1 = getMilitaryTime(hour: hour1)
            let militaryTime2 = getMilitaryTime(hour: hour2)
            return (militaryTime1 < militaryTime2)
        })
    }
    
    func constructHourListString() -> String {
        sortHours()
        var writeString = "["
        for i in 0..<self.hours.count {
            let hour = self.hours[i]
            let militaryTime = getMilitaryTime(hour: hour)
            writeString += String(militaryTime)
            if(i != self.hours.count-1) {
                writeString += ","
            }
        }
        writeString += "]"
        return writeString
    }
    
    @IBAction func donePressed(_ sender: Any) {
        if(self.hours.count != 0) {
            let writeString = constructHourListString()
            let data = writeString.data(using: String.Encoding.utf8)
            self.peripheral!.writeValue(data!, for: characteristic, type: CBCharacteristicWriteType.withResponse)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.hours.count < kMaxRowsPerScreen) {
            return kMaxRowsPerScreen
        } else {
            return self.hours.count
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "pickHourSegueIdentifier") {
            let dest = segue.destination as! HourPickerViewController
            dest.delegate = self
        }
    }
    
    func addHourlySchedule(hourString: String) {
        self.hours.append(hourString)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.hexStr("20AEE5")
        cell.textLabel?.textColor = UIColor.white
        if(indexPath.row < self.hours.count) {
            cell.textLabel?.text = self.hours[indexPath.row]
        } else {
            cell.textLabel?.text = nil
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete) {
            if(indexPath.row < self.hours.count) {
                self.hours.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
        }
    }
}
