//
//  PasswordViewController.swift
//  PillPopper User
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import SmileLock

class PasswordViewController: UIViewController, PasswordInputCompleteProtocol {
    
    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            validationSuccess()
        } else {
            validationFailure()
        }
    }
    
    @IBOutlet weak var passwordStackView: UIStackView!
    var passwordContainerView: PasswordContainerView!
    let kPasswordDigit = 4
    var incorrectAttempts = 0
    let kIncorrectAttemptsThreshold = 3
    var kLoginPassword:String! = nil
    
    override func viewDidLoad() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewDidLoad()
        passwordContainerView = PasswordContainerView.create(in: passwordStackView, digit: kPasswordDigit)
        passwordContainerView.delegate = self
        passwordContainerView.highlightedColor = UIColor.white
        passwordContainerView.tintColor = UIColor.color(.textColor)
        let prefs = UserDefaults.standard
        self.kLoginPassword = prefs.string(forKey: "userLoginPassword")
    }
    
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        if validation(input) {
            validationSuccess()
        }
        else {
            validationFailure()
        }
    }
    
    private func validation(_ input:String) -> Bool {
        if(kLoginPassword == nil) {
            return false
        } else {
            return input == kLoginPassword
        }
    }
    
    private func validationSuccess() {
        print("Login Successful")
        self.performSegue(withIdentifier: "loginSuccessfulSegue", sender: self)
    }
    
    private func validationFailure() {
        print("Incorrect Login")
        self.incorrectAttempts += 1
        passwordContainerView.wrongPassword()
        if(self.incorrectAttempts > kIncorrectAttemptsThreshold)
        {
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

