var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var PythonShell = require('python-shell');
var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'
var SNO_DELIM = ':';

var BlenoCharacteristic = bleno.Characteristic;

var ConfigureFPSCharacteristic = function() {

 ConfigureFPSCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb00',
    properties: ['write'],
  });

};

ConfigureFPSCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  console.log('ConfigureFPSCharacteristic - onWriteRequest: value = ' + data);
  
  // Will write S/N:TEXT -- I need to check if its the right S/N
  // to convert to string
  var dataString = data.toString('utf8');
  var dataSplit = dataString.split(":");
  var snoRecd = dataSplit[0];

  if (snoRecd != process.env.SERIAL_NO + '') {
    console.log('ConfigureFPSCharacteristic - Wrong Serial Number');
    callback(this.RESULT_FAILURE, 'Wrong Serial Number');
    return;
  }
  
  // write this to the file
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));

  if (configInfo.config_fps_proc == 1) {
    console.log('ConfigureFPSCharacteristic - In the middle of another configuring cycle');
    callback(this.RESULT_FAILURE, 'Cannot configure right now');
    return;
  }
  
  
  configInfo.config_fps_proc = 1;

  var writeback = JSON.stringify(configInfo, null, '\t');

  fs.writeFile(CONFIG_JSON_PATH, writeback, 
    function(err) {
      if(err) return console.error(err);
      console.log('ConfigureFPSCharacteristic - done writing to config file!!!');
  });

  var options = 
    { mode: 'text', 
      pythonPath:'/usr/bin/python', 
      pythonOptions:[], 
      scriptPath:'/home/pi/pi_comm/fps/', 
      args:[]};

  PythonShell.run('configure_fingerprint.py', options, 
    function(err, results) {
      if(err) throw err;
    });

  callback(this.RESULT_SUCCESS, 'DONE');
};

util.inherits(ConfigureFPSCharacteristic, BlenoCharacteristic);
module.exports = ConfigureFPSCharacteristic;
