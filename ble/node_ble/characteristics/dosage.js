var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var PythonShell = require('python-shell');
fs = require('fs');

var LOCKED = 1;
var UNLOCKED = 0;
var TIMEOUT_LOCK = 5; // in seconds
var SNO_DELIM = ':';
var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'


var BlenoCharacteristic = bleno.Characteristic;

var DosageCharacteristic = function() {

 DosageCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb03',
    properties: ['read', 'write'],
  });

  this._value = new Buffer(0);
};

DosageCharacteristic.prototype.onReadRequest = function(offset, callback) {

  console.log('DosageCharacteristic - onReadRequest');

  var sno = process.env.SERIAL_NO;

  // this is a string, and then I want to send this back to the user
  // now read the thing, and add the colon
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));

  var returnString = sno + SNO_DELIM + configInfo.dosage;

  console.log('DosageCharacteristic ' + returnString);
  this._value = new Buffer(returnString);

  // This is returning the SNO:DAY_FREQ_INT
  callback(this.RESULT_SUCCESS, this._value);
};

DosageCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  console.log('DosageCharacteristic - onWriteRequest: value = ' + data);
  var dosage = parseInt(data);
  // write this to the file
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));
  configInfo.dosage = dosage;
  var writeback = JSON.stringify(configInfo, null, '\t');

  fs.writeFile(CONFIG_JSON_PATH, writeback, 
    function(err) {
      if(err) return console.error(err);
      console.log('DosageCharacteristic - done writing to config file!!!');
    })


  callback(this.RESULT_SUCCESS, this._value.toString());
  
};

util.inherits(DosageCharacteristic, BlenoCharacteristic);
module.exports = DosageCharacteristic;
