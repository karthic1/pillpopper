//
//  HourPickerViewController.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit

class HourPickerViewController: UIViewController {

    @IBOutlet weak var datePickerView: UIDatePicker!
    
    @IBOutlet weak var hourText: UILabel!
    let dateFormatter = DateFormatter()
    var delegate:HourlyScheduleDelegate?
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if let del = self.delegate {
            del.addHourlySchedule(hourString: self.hourText.text!)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateFormatter.dateFormat = "hh:mm a"
        self.datePickerView.setValue(UIColor.white, forKey: "textColor")
        self.hourText.sizeToFit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func hourChanged(_ sender: Any) {
        let dateString = self.dateFormatter.string(from: self.datePickerView.date)
        let delimiters = CharacterSet(charactersIn: ": ")
        let components = dateString.components(separatedBy: delimiters)
        self.hourText.text = components[0] + ":00 " + components[2]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
