//
//  PharmacistOptionsTableViewController.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/16/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import CoreBluetooth

class PharmacistOptionsTableViewController: UITableViewController {
    
    var centralManager:CBCentralManager! = nil
    var peripheral:CBPeripheral! = nil
    var characteristicsDict:[CBUUID:PillPopperCharacteristic]! = nil
    let kDosageRow = 3
    let kLoadPillsRow = 2
    let kMaxRowsPerScreen = 15
    
    let tableDataSource = ["Weekly Schedule", "Hourly Schedule", "Load Pills", "Dosage"]
    
    func sendCharacteristicToDevice(key: CBUUID) {
        let c = self.characteristicsDict[key]
        if(key == CBUUID(string: PillPopperCharacteristic.uuidStrings.kDosageUUIDString.rawValue) || key == CBUUID(string: PillPopperCharacteristic.uuidStrings.kLoadPillsUUIDString.rawValue)) {
            let writeString = String(c?.value as! Int)
            let data = writeString.data(using: String.Encoding.utf8)
            self.peripheral!.writeValue(data!, for: (c?.characteristic!)!, type: CBCharacteristicWriteType.withResponse)
            if(c?.characteristic?.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kLoadPillsUUIDString.rawValue) {
                let topLockUUID = CBUUID(string: PillPopperCharacteristic.uuidStrings.kTopLockUUIDString.rawValue)
                let c = self.characteristicsDict[topLockUUID]
                let writeString = String("0")
                let data = writeString?.data(using: String.Encoding.utf8)
                self.peripheral!.writeValue(data!, for: (c?.characteristic!)!, type: CBCharacteristicWriteType.withResponse)
            }
        }
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if(peripheral?.state != .connected) {
            self.peripheral = nil
            return
        }
        centralManager.cancelPeripheralConnection(peripheral!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assert(self.centralManager != nil)
        assert(self.peripheral != nil)
        assert(self.characteristicsDict != nil)
        let titleDict = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kMaxRowsPerScreen
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pharmacistOptionsReuseIdentifier", for: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.hexStr("20AEE5")
        cell.textLabel?.textColor = UIColor.white
        cell.tintColor = UIColor.white
        if(indexPath.row < self.tableDataSource.count) {
            cell.textLabel?.text = self.tableDataSource[indexPath.row]
        } else {
            cell.textLabel?.text = nil
        }
        
        if(indexPath.row < 2) {
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == kDosageRow || indexPath.row == kLoadPillsRow) {
            let title = (indexPath.row == kDosageRow) ? "Dosage" : "Load Pills"
            let message = (indexPath.row == kDosageRow) ? "Enter Dosage in pills" : "Enter number of pills"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (_) in
                
                let characteristicUUID = CBUUID.init(string: indexPath.row == self.kDosageRow ? PillPopperCharacteristic.uuidStrings.kDosageUUIDString.rawValue : PillPopperCharacteristic.uuidStrings.kLoadPillsUUIDString.rawValue)
                let c = self.characteristicsDict[characteristicUUID]
                let value = Int((alertController.textFields?[0].text!)!)
                assert(value != nil)
                c!.updated = true
                c!.value = value
                self.characteristicsDict.updateValue(c!, forKey: characteristicUUID)
                self.sendCharacteristicToDevice(key: characteristicUUID)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            alertController.addTextField(configurationHandler: { (tf) in
                tf.keyboardType = UIKeyboardType.decimalPad
                tf.placeholder = (indexPath.row == self.kDosageRow) ? "Pill Dosage" : "Number of Pills"
                tf.addTarget(self, action: #selector(self.textChanged), for: UIControlEvents.editingChanged)
            })
            alertController.actions[0].isEnabled = false
            
            present(alertController, animated: true, completion: nil)
            self.tableView.deselectRow(at: indexPath, animated: true)
        } else if (indexPath.row == 0) {
            self.performSegue(withIdentifier: "weeklyScheduleSegueIdentifier", sender: self)
        } else if (indexPath.row == 1) {
            self.performSegue(withIdentifier: "hourlyScheduleSegueIdentifier", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "weeklyScheduleSegueIdentifier") {
            let vc = segue.destination as! WeeklyScheduleTableViewController
            vc.peripheral = self.peripheral
            vc.characteristic = self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleDayUUIDString.rawValue)]?.characteristic
        } else if (segue.identifier == "hourlyScheduleSegueIdentifier") {
            let vc = segue.destination as! HourlyScheduleTableViewController
            vc.peripheral = self.peripheral
            vc.characteristic = self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleHourUUIDString.rawValue)]?.characteristic
        }
    }
    
    func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        let value = Int((alert.textFields?[0].text!)!)
        alert.actions[0].isEnabled = (value != nil)
    }

}
