import spidev
import os
import time

import time
import os
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

# read SPI data from MCP3002 chip, 2 possible adc's (0 thru 1)
def adc_read(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 1) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low

        commandout = adcnum << 1
        commandout |= 0xD  # start bit + single-ended bit
        commandout <<= 4    # we only need to send 5 bits here
        for i in range(4):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # one null bit and 10 ADC bits
        for i in range(11):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)
        
        # first bit is 'null' so drop it
        adcout >>= 1
        return adcout

def spi_setup(spi_clk, spi_miso, spi_mosi, spi_cs):
 GPIO.setup(spi_mosi, GPIO.OUT)
 GPIO.setup(spi_miso,GPIO.IN)
 GPIO.setup(spi_clk, GPIO.OUT)
 GPIO.setup(spi_cs, GPIO.OUT) 

def setup():
  SPICLK = 11
  SPIMISO = 9
  SPIMOSI = 10
  SPICS = 8
  PHOTO_CHANNEL = 0
  spi_setup(SPICLK, SPIMISO, SPIMOSI, SPICS)
