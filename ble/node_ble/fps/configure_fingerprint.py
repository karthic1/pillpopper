import fps
import json

#usage: fps.registerNewFingerprint(), 
#fps.verifyFingerprint(), 
#fps.deleteFingerprint(templateNumber)

fps.registerNewFingerprint()

with open('/home/pi/config/pill_info.json') as data_file:    
        data = json.load(data_file)

data["config_fps_proc"] = 0

with open('/home/pi/config/pill_info.json', 'w') as outfile:
        json.dump(data, outfile)
