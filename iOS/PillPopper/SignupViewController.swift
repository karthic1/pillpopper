//
//  SignupViewController.swift
//  PillPopper
//
//  Created by Karthic on 4/10/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SignupViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var pharmacistIDTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    var chosenTextField:UITextField! = nil
    let globalPharmacistRef = FIRDatabase.database().reference(withPath: "pharmacists")
    var createdPharmacistRef:FIRDatabaseReference! = nil
    var createdPharmacistStruct:Pharmacist! = nil
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }


    @IBAction func signupButtonPressed(_ sender: Any) {
        let name = nameTextField.text
        let id = pharmacistIDTextField.text
        if(name == "") {
            self.errorLabel.text = "Please enter name"
            self.errorLabel.isHidden = false
        } else if((id == "") || (Int(id!) == nil)) {
            self.errorLabel.text = "Please enter a valid ID"
            self.errorLabel.isHidden = false
        } else {
            let pharmacist = Pharmacist(name: name!, id: Int(id!)!)
            let pharmacistIdString = String(describing: Int(id!))
            let pharmacistRef = self.globalPharmacistRef.child(pharmacistIdString)
            globalPharmacistRef.observeSingleEvent(of: .value, with: { (snapshot) in
                if (snapshot.hasChild(pharmacistIdString)) {
                    self.errorLabel.text = "User already exists"
                    self.errorLabel.isHidden = false
                } else {
                    pharmacistRef.setValue(pharmacist.toAnyObject())
                    self.createdPharmacistRef = pharmacistRef
                    self.createdPharmacistStruct = pharmacist
                    self.performSegue(withIdentifier: "createPasswordSegueIdentifier", sender: self)
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "createPasswordSegueIdentifier") {
            let dest = segue.destination as! SignupPasswordViewController
            assert(self.createdPharmacistRef != nil && self.createdPharmacistStruct != nil)
            dest.pharmacistRef = self.createdPharmacistRef
            dest.pharmacistStruct = self.createdPharmacistStruct
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(!self.errorLabel.isHidden) {
            self.errorLabel.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        self.nameTextField.delegate = self
        self.pharmacistIDTextField.delegate = self
        self.errorLabel.sizeToFit()
        super.viewDidLoad()
        let titleDict = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.nameTextField.resignFirstResponder()
        self.pharmacistIDTextField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
