//
//  UserClass.swift
//  PillPopper User
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import Foundation
import FirebaseDatabase

class User {
    var deviceID: String
    var name: String
    var ref:FIRDatabaseReference?
    var password: String?
    var updatedPassword: Bool
    
    init(name: String, deviceId: String) {
        self.name = name
        self.deviceID = deviceId
        self.ref = nil
        self.password = nil
        self.updatedPassword = false
    }
    
    init(data: [String:AnyObject], name: String, ref: FIRDatabaseReference) {
        self.deviceID = data["deviceID"] as! String
        self.password = nil
        self.updatedPassword = data["updatedPassword"] as! Bool
        self.name = name
        self.ref = ref
    }
    
    func toAnyObject() -> Any {
        return [
            "deviceID": deviceID,
            "name": name,
            "updatedPassword": updatedPassword,
            "password": password as Any
        ]
    }
}
