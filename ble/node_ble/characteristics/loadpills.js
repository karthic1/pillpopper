var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var PythonShell = require('python-shell');
fs = require('fs');

var LOCKED = 1;
var UNLOCKED = 0;
var TIMEOUT_LOCK = 5; // in seconds
var SNO_DELIM = ':';
var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'


var BlenoCharacteristic = bleno.Characteristic;

var LoadPillsCharacteristic = function() {

 LoadPillsCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb02',
    properties: ['write'],
  });

 this._value = new Number(LOCKED);
};

LoadPillsCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  console.log('LoadPillsCharacteristic - onWriteRequest: value = ' + data);
  var toLoad = parseInt(data);
  // write this to the file
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));
  configInfo.pills_left = configInfo.pills_left + toLoad;
  var writeback = JSON.stringify(configInfo, null, '\t');

  fs.writeFile(CONFIG_JSON_PATH, writeback, 
    function(err) {
      if(err) return console.error(err);
      console.log('LoadPillsCharacteristic - done writing to config file!!!');
    })


  callback(this.RESULT_SUCCESS, this._value.toString());
  
};

util.inherits(LoadPillsCharacteristic, BlenoCharacteristic);
module.exports = LoadPillsCharacteristic;
