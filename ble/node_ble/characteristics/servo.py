import time
import wiringpi
import mcp3002
import json
import fps
import ctypes

_acc = ctypes.CDLL('/home/pi/pi_comm/acc/acc.so')
_acc.main.argtypes = ()

def acc_is_upright():
    global _acc
    result = _acc.main()
    if (int(result) == 0):
        return False
    else: 
        return True

with open('/home/pi/config/pill_info.json') as data_file:    
    data = json.load(data_file)

dispense_left = int(data["dispense_left"])

wiringpi.wiringPiSetupGpio()
wiringpi.pinMode(18, wiringpi.GPIO.PWM_OUTPUT)
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)
wiringpi.pwmSetClock(192)
wiringpi.pwmSetRange(2000)
wiringpi.pwmWrite(18,150) #ensure that motor is stopped by default 
mcp3002.setup()
SPICLK = 11
SPIMISO = 9
SPIMOSI = 10
SPICS = 8
PHOTO_CHANNEL = 0
#time in seconds to wait (2 minutes)
PILL_WAIT_THRESHOLD = 120
#threshold values for which a pill is on the dispensing tray
DARKNESS_THRESHOLD = 900
#time in seconds to wait (1 minute)
MOTOR_WAIT_THRESHOLD = 60

def pill_in_tray():
    adc_val = mcp3002.adc_read(PHOTO_CHANNEL, SPICLK, SPIMOSI, SPIMISO, SPICS) 
    return adc_val > DARKNESS_THRESHOLD

#fps.verifyFingerprint will return false if the user does not authenticate 
#within 2 minutes, or if the fingerprints do not match.
#if (acc_is_upright() and fps.verifyFingerprint()):
print "about to check fingerprint and accel orientation" 
print acc_is_upright()
if (acc_is_upright() and fps.verifyFingerprint()):
    print 'verified finger'
    init_time = time.time()
    #if there is already a pill in the tray, wait for the person to withdraw the
    #pill for two minutes. If they don't, nothing happens
    print 'about to check if pill in tray'
    while(pill_in_tray()):
        print 'pill in tray'
        if(time.time() - init_time >= PILL_WAIT_THRESHOLD):
            print 'timed out waiting for pill to be out of tray'
            exit(0)
    print 'pill not in tray'
    #dispense dispense_left amount of pills
    for i in xrange(dispense_left):
        timeoutMotorTurn = False
        timeoutPillTake = False
        motor_init_time = time.time()
        motor_round_time = time.time()
        print("Dispense_left = " + str(dispense_left) + "\n")
        #spin motor until either it times out, or because a pill is dropped into
        #the dispensing area
        while True:
            wiringpi.pwmWrite(18, 142) 
            if (time.time() - motor_round_time >= 1.1):
                wiringpi.pwmWrite(18, 150) 
                time.sleep(1)
                motor_round_time = time.time()

            #stop servo if it has spun more than a minute 

            if (time.time() - motor_init_time >= MOTOR_WAIT_THRESHOLD):
                print 'stopping servo bc timeout'
                wiringpi.pwmWrite(18, 150)
                timeoutMotorTurn = True
                break
            #stop servo if it has dispensed a pill
            if(pill_in_tray()):
                print 'stopping servo'
                wiringpi.pwmWrite(18, 150)
                break    
        if (not timeoutMotorTurn):
            #pill dispensed 
            data["dispense_left"] = data["dispense_left"] - 1
            data["pills_left"] = data["pills_left"] - 1 
            #wait for user to take out the pill
            init_time = time.time()
            print 'waiting for tablet removal ' + str(i) + '\n'
            while True: 
                
                if(time.time() - init_time >= PILL_WAIT_THRESHOLD):
                    timeoutPillTake = True
                    break
                if(not pill_in_tray()):
                    time.sleep(5)
                    break

            if timeoutPillTake:
                break

        else:
            # motor turning timed out
            # we did not dispense any pills
            break

data["dispense_proc"] = 0

with open('/home/pi/config/pill_info.json', 'w') as outfile:
    json.dump(data, outfile)

