//
//  UserOptionsTableViewController.swift
//  PillPopper User
//
//  Created by Karthic on 4/18/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import CoreBluetooth
import EventKit

class UserOptionsTableViewController: UITableViewController, CBPeripheralDelegate {
    
    var centralManager:CBCentralManager! = nil
    var peripheral:CBPeripheral! = nil
    var characteristicsDict:[CBUUID:PillPopperCharacteristic]! = nil
    let kSyncScheduleRow = 0
    let kCheckPillsRow = 1
    let kDosageRow = 2
    let kConfigureFPSRow = 3
    let kDispensePillsRow = 4
    var kDeviceID:String = ""
    let kDeviceIDLength = 6
    let kMaxRowsPerScreen = 15
    let kMinutesInSeconds = 60
    let kHourInSeconds = 60 * 60
    let kDayInSeconds = 24 * 60 * 60
    
    let tableDataSource = ["Sync Schedule", "Pills Left", "Dosage", "Configure Fingerprint", "Dispense Pills"]
    
    func sendCharacteristicToDevice(key: CBUUID) {
        let c = self.characteristicsDict[key]
        let writeString = kDeviceID + ":" + String(c?.value as! Int)
        print(writeString)
        let data = writeString.data(using: String.Encoding.utf8)
        self.peripheral!.writeValue(data!, for: (c?.characteristic!)!, type: CBCharacteristicWriteType.withResponse)
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if(peripheral?.state != .connected) {
            self.peripheral = nil
            return
        }
        centralManager.cancelPeripheralConnection(peripheral!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assert(self.centralManager != nil)
        assert(self.peripheral != nil)
        assert(self.characteristicsDict != nil)
        let titleDict = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict
        self.navigationController?.navigationBar.isTranslucent = false
        let prefs = UserDefaults.standard
        self.kDeviceID = prefs.string(forKey: "deviceID")!
        assert(self.kDeviceID.characters.count == kDeviceIDLength)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func syncSchedule() -> Int {
        let eventStore:EKEventStore = EKEventStore()
        var status = 0
        eventStore.requestAccess(to: EKEntityType.event) { (granted, error) in
            if(granted && error == nil) {
                let dayFreq = self.characteristicsDict[CBUUID.init(string:PillPopperCharacteristic.uuidStrings.kScheduleDayUUIDString.rawValue)]?.value as! Int
                let hourFreq = self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleHourUUIDString.rawValue)]?.value as! [Int]
                
                for hour in hourFreq {
                    let recurrenceRule = EKRecurrenceRule(recurrenceWith: EKRecurrenceFrequency.daily, interval: dayFreq, end: nil)
                    
                    let event = EKEvent(eventStore: eventStore)
                    event.title = "PillPopper Reminder"
                    event.notes = "Time to take your pills"
                    let calendar = Calendar.current
                    let comp = calendar.dateComponents(Set(arrayLiteral: .calendar, .timeZone, .era, .year, .month, .day, .weekday, .weekdayOrdinal, .quarter, .weekOfMonth, .weekOfYear, .yearForWeekOfYear), from: Date())
                    let todayMidnight = calendar.date(from: DateComponents(calendar: comp.calendar, timeZone: comp.timeZone, era: comp.era, year: comp.year, month: comp.month, day: comp.day, hour: 0, minute: 0, second: 0, nanosecond: 0, weekday: comp.weekday, weekdayOrdinal: comp.weekdayOrdinal, quarter: comp.quarter, weekOfMonth: comp.weekOfMonth, weekOfYear: comp.weekOfYear, yearForWeekOfYear: comp.yearForWeekOfYear))!
                    
                    let startDate = todayMidnight.addingTimeInterval(TimeInterval(hour * self.kHourInSeconds))
                    let endDate = startDate.addingTimeInterval(TimeInterval(self.kMinutesInSeconds * 10))
                    event.startDate = startDate
                    event.endDate = endDate
                    event.addRecurrenceRule(recurrenceRule)
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    do {
                        try eventStore.save(event, span: EKSpan.thisEvent)
                    } catch {
                        print("Calendar could not save event \(error.localizedDescription)")
                    }
                }
            }
            else {
                print("Access not granted \(String(describing: error?.localizedDescription))")
                status = 1
            }
        }
        return status
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kMaxRowsPerScreen
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userOptionsReuseIdentifier", for: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        cell.backgroundColor = UIColor.hexStr("20AEE5")
        cell.textLabel?.textColor = UIColor.white
        cell.detailTextLabel?.textColor = UIColor.white
        if(indexPath.row < self.tableDataSource.count) {
            cell.textLabel?.text = self.tableDataSource[indexPath.row]
        } else {
            cell.textLabel?.text = nil
        }
        
        cell.detailTextLabel?.text = nil
        if(indexPath.row == kCheckPillsRow || indexPath.row == kDosageRow) {
            var uuid:CBUUID
            if(indexPath.row == kDosageRow) {
                uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kDosageUUIDString.rawValue)
            } else {
                uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kPillsLeftUUIDString.rawValue)
            }
            let val = self.characteristicsDict[uuid]
            if(val?.value != nil) {
                cell.detailTextLabel?.text = String(val?.value as! Int) + " pills"
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == kSyncScheduleRow) {
            var uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleDayUUIDString.rawValue)
            var val = self.characteristicsDict[uuid]
            val!.value = -1
            self.peripheral.readValue(for: val!.characteristic!)
            
            uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleHourUUIDString.rawValue)
            val = self.characteristicsDict[uuid]
            val!.value = []
            self.peripheral.readValue(for: val!.characteristic!)
        }
        else if(indexPath.row == kCheckPillsRow || indexPath.row == kDosageRow) {
            var uuid:CBUUID
            if(indexPath.row == kCheckPillsRow) {
                uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kPillsLeftUUIDString.rawValue)
            } else {
                uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kDosageUUIDString.rawValue)
            }
            let val = self.characteristicsDict[uuid]
            self.peripheral!.readValue(for: val!.characteristic!)
        } else if(indexPath.row == kConfigureFPSRow) {
            let uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kConfigureFpsUUIDString.rawValue)
            let val = self.characteristicsDict[uuid]
            val!.value = 1
            self.characteristicsDict.updateValue(val!, forKey: uuid)
            sendCharacteristicToDevice(key: uuid)
        } else if(indexPath.row == kDispensePillsRow) {
            let uuid = CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kDispenseUUIDString.rawValue)
            let val = self.characteristicsDict[uuid]
            val!.value = 1
            self.characteristicsDict.updateValue(val!, forKey: uuid)
            sendCharacteristicToDevice(key: uuid)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func processScheduleResponse(characteristic: CBCharacteristic) {
        let val = self.characteristicsDict[characteristic.uuid]
        let response = String.init(data: characteristic.value!, encoding: String.Encoding.utf8)
        print("response " + response!)
        let components = response?.components(separatedBy: CharacterSet.init(charactersIn: ":"))
        //        assert(components[0].characters.count == kDeviceIDLength)
        
        
        if(characteristic.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kScheduleDayUUIDString.rawValue) {
            val!.value = Int((components?[1])!)!
        } else if(characteristic.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kScheduleHourUUIDString.rawValue) {
            let hoursList = components?[1].components(separatedBy: ",").map({ (hourString) -> Int in
                return Int(hourString)!
            })
            assert((hoursList?.count)! > 0)
            val!.value = hoursList
            let ret = syncSchedule()
            let alertController = UIAlertController(title: "PillPopper", message: (ret == 1) ? "Error updating calendar" : "Successfully updated calendar", preferredStyle: UIAlertControllerStyle.alert)
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)

        }
        self.characteristicsDict.updateValue(val!, forKey: characteristic.uuid)
    }
    
    func processPillResponse(characteristic: CBCharacteristic) {
        let response = String.init(data: characteristic.value!, encoding: String.Encoding.utf8)
        let components = response?.components(separatedBy: CharacterSet.init(charactersIn: ":"))
        //        assert(components[0].characters.count == kDeviceIDLength)

        if(characteristic.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kDosageUUIDString.rawValue) {
            let val = self.characteristicsDict[characteristic.uuid]
            val?.value = Int(components![1])
            self.characteristicsDict.updateValue(val!, forKey: characteristic.uuid)
            
            let indexPath = IndexPath(row: 2, section: 0)
            let cell = self.tableView.cellForRow(at: indexPath)
            cell?.detailTextLabel?.text = (components?[1])! + " pills"
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        } else if(characteristic.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kPillsLeftUUIDString.rawValue) {
            let val = self.characteristicsDict[characteristic.uuid]
            val?.value = Int(components![1])
            self.characteristicsDict.updateValue(val!, forKey: characteristic.uuid)
            
            let indexPath = IndexPath(row: 1, section: 0)
            let cell = self.tableView.cellForRow(at: indexPath)
            cell?.detailTextLabel?.text = (components?[1])! + " pills"
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if(characteristic.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kScheduleDayUUIDString.rawValue || characteristic.uuid.uuidString == PillPopperCharacteristic.uuidStrings.kScheduleHourUUIDString.rawValue) {
            processScheduleResponse(characteristic: characteristic)
        } else {
            processPillResponse(characteristic: characteristic)
        }
    }
}
