//
//  PharmacistClass.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/16/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Pharmacist {
    var id: Int
    var name: String
    var ref:FIRDatabaseReference?
    var password: String?
    
    init(name: String, id: Int) {
        self.name = name
        self.id = id
        self.ref = nil
        self.password = nil
    }
    
    func toAnyObject() -> Any {
        return [
            "id": id,
            "name": name,
            "password": password as Any
        ]
    }
}
