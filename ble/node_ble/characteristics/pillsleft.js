var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var PythonShell = require('python-shell');
fs = require('fs');

var LOCKED = 1;
var UNLOCKED = 0;
var TIMEOUT_LOCK = 5; // in seconds
var SNO_DELIM = ':';
var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'


var BlenoCharacteristic = bleno.Characteristic;

var PillsLeftCharacteristic = function() {

 PillsLeftCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb01',
    properties: ['read'],
  });

 this._value = new Number(LOCKED);
};

PillsLeftCharacteristic.prototype.onReadRequest = function(offset, callback) {

  console.log('PillsLeftCharacteristic - onReadRequest');

  var sno = process.env.SERIAL_NO;
  // this is a string, and then I want to send this back to the user
  // now read the thing, and add the colon
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));

  var returnString = sno + SNO_DELIM + configInfo.pills_left;

  console.log('PillsLeftCharacteristic ' + returnString);

  // This is returning the SNO:DAY_FREQ_INT
  callback(this.RESULT_SUCCESS, Buffer.from(returnString, 'utf8'));
};

util.inherits(PillsLeftCharacteristic, BlenoCharacteristic);
module.exports = PillsLeftCharacteristic;
