//
//  RegisterPasswordViewController.swift
//  PillPopper User
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import SmileLock
import FirebaseDatabase

class RegisterPasswordViewController: UIViewController, PasswordInputCompleteProtocol {

    var userRef:FIRDatabaseReference! = nil
    var userStruct:User! = nil
    @IBOutlet weak var passwordStackView: UIStackView!
    var passwordContainerView: PasswordContainerView!
    let kPasswordDigit = 4
    var newPassword:String = ""
    var passwordEntered:Bool = false
    
    @IBOutlet weak var passwordCreationStatusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        assert(self.userRef != nil && self.userStruct != nil)
        passwordContainerView = PasswordContainerView.create(in: passwordStackView, digit: kPasswordDigit)
        passwordContainerView.delegate = self
        passwordContainerView.highlightedColor = UIColor.white
        passwordContainerView.tintColor = UIColor.color(.textColor)
    }
    
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        
        if(passwordEntered == false) {
            passwordEntered = true
            newPassword = input
            self.passwordCreationStatusLabel.text = "Confirm Password"
            self.passwordCreationStatusLabel.sizeToFit()
            self.passwordContainerView.clearInput()
        } else {
            if(input == newPassword) {
                //update the database
                self.userStruct.password = input
                self.userStruct.updatedPassword = true
                self.userRef.setValue(self.userStruct.toAnyObject())
                //cache the password credentials locally
                let prefs = UserDefaults.standard
                prefs.setValue(input, forKey: "userLoginPassword")
                prefs.setValue(self.userStruct.deviceID, forKey: "deviceID")
                dismiss(animated: true, completion: nil)
            } else {
                self.passwordContainerView.wrongPassword()
                self.newPassword = ""
                self.passwordEntered = false
                self.passwordCreationStatusLabel.text = "Create Password"
                self.passwordCreationStatusLabel.sizeToFit()
            }
        }
    }
    
    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
