var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var PythonShell = require('python-shell');
fs = require('fs');

var LOCKED = 1;
var UNLOCKED = 0;
var TIMEOUT_LOCK = 5; // in seconds
var SNO_DELIM = ':';
var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'

var BlenoCharacteristic = bleno.Characteristic;

var ScheduleDayFreqCharacteristic = function() {

 ScheduleDayFreqCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb05',
    properties: ['read', 'write'],
  });

 this._value = new Number(LOCKED);
};

ScheduleDayFreqCharacteristic.prototype.onReadRequest = function(offset, callback) {

  console.log('ScheduleDayFreqCharacteristic - onReadRequest');

  var sno = process.env.SERIAL_NO;
  // this is a string, and then I want to send this back to the user
  // now read the thing, and add the colon
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));

  var returnString = sno + SNO_DELIM + configInfo.day_freq;

  console.log('ScheduleDayFreqCharacteristic ' + returnString);

  // This is returning the SNO:DAY_FREQ_INT
  callback(this.RESULT_SUCCESS, Buffer.from(returnString, 'utf8'));
};

ScheduleDayFreqCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  console.log('ScheduleDayFreqCharacteristic - onWriteRequest: value = ' + data);
  var day_freq = parseInt(data);
  // write this to the file
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));
  configInfo.day_freq = day_freq;
  var writeback = JSON.stringify(configInfo, null, '\t');

  fs.writeFile(CONFIG_JSON_PATH, writeback, 
    function(err) {
      if(err) return console.error(err);
      console.log('ScheduleDayFreqCharacteristic - done writing to config file!!!');
    })


  callback(this.RESULT_SUCCESS, this._value.toString());
  
};

util.inherits(ScheduleDayFreqCharacteristic, BlenoCharacteristic);
module.exports = ScheduleDayFreqCharacteristic;
