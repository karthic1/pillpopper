var bleno = require('bleno');
var util = require('util');

var ToplockCharacteristic = require('./characteristics/toplock');
var DispenseCharacteristic = require('./characteristics/dispense');
var ScheduleDayFreqCharacteristic = require('./characteristics/schedule_dayfreq');
var ScheduleHoursCharacteristic = require('./characteristics/schedule_hours');
var DosageCharacteristic = require('./characteristics/dosage');
var LoadPillsCharacteristic = require('./characteristics/loadpills');
var PillsLeftCharacteristic = require('./characteristics/pillsleft');
var ConfigureFPSCharacteristic = require('./characteristics/configure_fps');
var UptimeCharacteristic = require('./characteristics/uptime');


function SystemInformationService() {

  bleno.PrimaryService.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb07',
    characteristics: [
      new ToplockCharacteristic(),          // ff51b30e-d7e2-4d93-8842-a7c4a57dfb07
      new DispenseCharacteristic(),         // ff51b30e-d7e2-4d93-8842-a7c4a57dfb06
      new ScheduleDayFreqCharacteristic(),  // ff51b30e-d7e2-4d93-8842-a7c4a57dfb05
      new ScheduleHoursCharacteristic(),    // ff51b30e-d7e2-4d93-8842-a7c4a57dfb04
      new DosageCharacteristic(),           // ff51b30e-d7e2-4d93-8842-a7c4a57dfb03
      new LoadPillsCharacteristic(),        // ff51b30e-d7e2-4d93-8842-a7c4a57dfb02
      new PillsLeftCharacteristic(),        // ff51b30e-d7e2-4d93-8842-a7c4a57dfb01
      new ConfigureFPSCharacteristic(),      // ff51b30e-d7e2-4d93-8842-a7c4a57dfb00
      new UptimeCharacteristic()
    ]
  });
};

util.inherits(SystemInformationService, bleno.PrimaryService);
module.exports = SystemInformationService;
