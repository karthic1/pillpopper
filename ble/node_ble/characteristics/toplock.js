var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var PythonShell = require('python-shell');
var LOCKED = 1;
var UNLOCKED = 0;
var TIMEOUT_LOCK = 5; // in seconds

var BlenoCharacteristic = bleno.Characteristic;

var ToplockCharacteristic = function() {

 ToplockCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb07',
    properties: ['write'],
  });

 this._value = new Number(LOCKED);
};

ToplockCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  console.log('ToplockCharacteristic - onWriteRequest: value = ' + data);
  var l = parseInt(data);
  if (l == UNLOCKED) {
    // means I want to now unlock the solenoid
    console.log('ToplockCharacteristic - run unlock_solenoid.py');
    this._value = UNLOCKED;
    var options = 
    { mode: 'text', 
      pythonPath:'/usr/bin/python', 
      pythonOptions:[], 
      scriptPath:'/home/pi/evothings-examples/node_ble/characteristics', 
      args:[]};

    PythonShell.run('unlock_solenoid.py', options, function(err, results) {
      if(err) throw err;
    });
  }

  callback(this.RESULT_SUCCESS, this._value.toString());
  // now wait for 5 seconds and then lock it again
  // WAIT FOR A WHILE NOW
  setTimeout(function() {
    console.log('ToplockCharacteristic - time to lock - run lock_solenoid.py');
    this._value = LOCKED;
    PythonShell.run('lock_solenoid.py', options, function(err, results) {
      if(err) throw err;
    });
    console.log('ToplockCharacteristic - locked again sorry :( ');
  }, TIMEOUT_LOCK * 1000);
};

util.inherits(ToplockCharacteristic, BlenoCharacteristic);
module.exports = ToplockCharacteristic;
