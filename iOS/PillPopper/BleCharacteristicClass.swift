//
//  BleCharacteristicClass.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/16/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import Foundation
import CoreBluetooth

class PillPopperCharacteristic {
    
    enum uuidStrings: String {
        case kConfigureFpsUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB00"
        case kPillsLeftUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB01"
        case kLoadPillsUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB02"
        case kDosageUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB03"
        case kScheduleHourUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB04"
        case kScheduleDayUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB05"
        case kDispenseUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB06"
        case kTopLockUUIDString = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB07"
    }
    
    static let kServiceUUIDString:String = "FF51B30E-D7E2-4D93-8842-A7C4A57DFB07"
    var characteristic: CBCharacteristic?
    var updated:Bool
    var value:Any?
    
    init(characteristic: CBCharacteristic?) {
        self.characteristic = characteristic
        self.updated = false
        self.value = nil
    }
}
    
