//
//  RegisterViewController.swift
//  PillPopper User
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import FirebaseDatabase

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var deviceIDTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    var chosenTextField:UITextField! = nil
    let globalUserRef = FIRDatabase.database().reference(withPath: "users")
    var createdUserRef:FIRDatabaseReference! = nil
    var createdUserStruct:User! = nil
    let kDeviceIDLength = 6
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        let name = nameTextField.text
        let id = deviceIDTextField.text
        if(name == "") {
            self.errorLabel.text = "Please enter name"
            self.errorLabel.isHidden = false
        } else if(id == "" || id?.characters.count != kDeviceIDLength ) {
            self.errorLabel.text = "Please enter a valid device ID"
            self.errorLabel.isHidden = false
        } else {
            //check if the device ID exists in the database and the password for the device hasn't yet been updated
            globalUserRef.observeSingleEvent(of: .value, with: { (snapshot) in
                //if the list of users has this Device ID
                if(snapshot.hasChild(id!)) {
                    assert(snapshot.hasChildren())
                    let childSnapshot = snapshot.childSnapshot(forPath: id!)
                    let userDatabaseStore = childSnapshot.value as? [String: AnyObject] ?? [:]
                    let updatedPassword = userDatabaseStore["updatedPassword"] as! Bool
                    if(updatedPassword) {
                        self.errorLabel.text = "Device already registered"
                        self.errorLabel.isHidden = false
                    } else {
                        let existingUserStruct = User(data: userDatabaseStore, name: name!, ref: childSnapshot.ref)
                        self.createdUserStruct = existingUserStruct
                        self.createdUserRef = existingUserStruct.ref!
                        self.performSegue(withIdentifier: "createPasswordSegueIdentifier", sender: self)
                    }
                } else {
                    self.errorLabel.text = "Device ID not found"
                    self.errorLabel.isHidden = false
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "createPasswordSegueIdentifier") {
            let dest = segue.destination as! RegisterPasswordViewController
            assert(self.createdUserRef != nil && self.createdUserStruct != nil)
            dest.userRef = self.createdUserRef
            dest.userStruct = self.createdUserStruct
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(!self.errorLabel.isHidden) {
            self.errorLabel.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        self.nameTextField.delegate = self
        self.deviceIDTextField.delegate = self
        self.errorLabel.sizeToFit()
        super.viewDidLoad()
        let titleDict = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.nameTextField.resignFirstResponder()
        self.deviceIDTextField.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
