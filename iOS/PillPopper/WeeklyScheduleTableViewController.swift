//
//  WeeklyScheduleTableViewController.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/17/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import CoreBluetooth

class WeeklyScheduleTableViewController: UITableViewController {
    
    var selectedIndexPath:IndexPath! = nil
    var peripheral:CBPeripheral! = nil
    var weeklyFrequency:Int = 0
    var characteristic:CBCharacteristic! = nil
    let kMaxRowsPerScreen = 15

    let tableDataSource = ["Every Day", "Every 2 Days", "Every Week", "Custom"]

    @IBAction func doneButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(peripheral != nil)
        assert(characteristic != nil)
        // Uncomment the following line to preserve selection between presentations
         self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.selectedIndexPath != nil) {
            self.tableView.cellForRow(at: self.selectedIndexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.weeklyFrequency != 0) {
            let writeString = String(self.weeklyFrequency)
            let data = writeString.data(using: String.Encoding.utf8)
            self.peripheral!.writeValue(data!, for: characteristic, type: CBCharacteristicWriteType.withResponse)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kMaxRowsPerScreen
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weeklyScheduleOptionReuseIdentifier", for: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.hexStr("20AEE5")
        cell.textLabel?.textColor = UIColor.white
        cell.tintColor = UIColor.white
        if(indexPath.row < self.tableDataSource.count) {
            cell.textLabel?.text = self.tableDataSource[indexPath.row]
        } else {
            cell.textLabel?.text = nil
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0) {
            self.weeklyFrequency = 1
        } else if (indexPath.row == 1) {
            self.weeklyFrequency = 2
        } else if (indexPath.row == 2) {
            self.weeklyFrequency = 7
        }
        if(self.selectedIndexPath != nil) {
            self.tableView.cellForRow(at: self.selectedIndexPath)?.accessoryType = UITableViewCellAccessoryType.none
        }
        self.selectedIndexPath = indexPath
        self.tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
        if(indexPath.row == 3) {
            let title = "Weekly Frequency"
            let message = "Enter frequency in days"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (_) in
                let value = Int((alertController.textFields?[0].text!)!)
                assert(value != nil)
                self.weeklyFrequency = value!
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            alertController.addTextField(configurationHandler: { (tf) in
                tf.keyboardType = UIKeyboardType.decimalPad
                tf.placeholder = "Weekly Frequency"
                tf.addTarget(self, action: #selector(self.textChanged), for: UIControlEvents.editingChanged)
            })
            alertController.actions[0].isEnabled = false
            present(alertController, animated: true, completion: nil)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        let value = Int((alert.textFields?[0].text!)!)
        alert.actions[0].isEnabled = (value != nil)
    }


    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
