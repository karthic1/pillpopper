var bleno = require('bleno');
var os = require('os');
var util = require('util');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var PythonShell = require('python-shell');

var CONFIG_JSON_PATH = '/home/pi/config/pill_info.json'
var SNO_DELIM = ':';

var BlenoCharacteristic = bleno.Characteristic;

var DispenseCharacteristic = function() {

 DispenseCharacteristic.super_.call(this, {
    uuid: 'ff51b30e-d7e2-4d93-8842-a7c4a57dfb06',
    properties: ['write'],
  });

};

DispenseCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  // Check 
  // 1. orientation is right
  // 2. FPS 
  // 3. Check no pills already dispensed

  console.log('DispenseCharacteristic - onWriteRequest: value = ' + data);
    // means I want to now unlock the solenoid
  console.log('DispenseCharacteristic - run rotate_motor.py');

  // Will write S/N:TEXT -- I need to check if its the right S/N
  var dataString = data.toString('utf8');
  var dataSplit = dataString.split(":");
  var snoRecd = dataSplit[0];
  console.log('DispenseChar serial number = ' + process.env.SERIAL_NO);

  if (snoRecd != process.env.SERIAL_NO + '') {
   console.log('DispenseCharacteristic - Wrong Serial Number');
   callback(this.RESULT_FAILURE, 'Wrong Serial Number');
   return;
  }

  // write this to the file
  var configInfo = JSON.parse(fs.readFileSync(CONFIG_JSON_PATH, 'utf8'));
  if (configInfo.dispense_left == 0) {
    console.log('DispenseCharacteristic - Not time to take yo pills');
    callback(this.RESULT_FAILURE, 'Not time to take yo pills');
    return;
  } else if (configInfo.pills_left < configInfo.dispense_left) {
    // Not enough pills left
    console.log('DispenseCharacteristic - Not enough pills');
    callback(this.RESULT_FAILURE, 'Not enough pills left');
    return;
  } else if (configInfo.dispense_proc == 1) {
    console.log('DispenseCharacteristic - Dispensing rn');
    callback(this.RESULT_FAILURE, 'Kindly have patience darling');
    return;
  }

  configInfo.dispense_proc = 1
  
  var writeback = JSON.stringify(configInfo, null, '\t');

  fs.writeFile(CONFIG_JSON_PATH, writeback, 
    function(err) {
      if(err) return console.error(err);
      console.log('DispenseCharacteristic - done writing to config file!!!');
  })

  var options = 
    { mode: 'text', 
      pythonPath:'/usr/bin/python', 
      pythonOptions:[], 
      scriptPath:'/home/pi/evothings-examples/node_ble/characteristics/', 
      args:[]};

  PythonShell.run('servo.py', options, 
    function(err, results) {
      if(err) return console.error(err);
    });

  callback(this.RESULT_SUCCESS, 'DONE');
};

util.inherits(DispenseCharacteristic, BlenoCharacteristic);
module.exports = DispenseCharacteristic;
