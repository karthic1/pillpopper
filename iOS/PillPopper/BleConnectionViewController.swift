//
//  BleConnectionViewController.swift
//  PillPopper Pharm
//
//  Created by Karthic on 4/16/17.
//  Copyright © 2017 Karthic. All rights reserved.
//

import UIKit
import CoreBluetooth

class BleConnectionViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager:CBCentralManager!
    var peripheral:CBPeripheral?
    var disconnecting = false
    
    var characteristicsDict:[CBUUID:PillPopperCharacteristic] = [:]
    let kNumTotalCharacteristics:Int = 5
    
    @IBOutlet weak var bleStatusLabel: UILabel!
    @IBOutlet weak var bleActivityIndicator: UIActivityIndicatorView!
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if(peripheral?.state != .connected) {
            self.peripheral = nil
            return
        }
        self.disconnecting = true
        centralManager.cancelPeripheralConnection(peripheral!)
    }
    
    func initializeCharacteristicsDict() {
        self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kLoadPillsUUIDString.rawValue)] = nil
        self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kTopLockUUIDString.rawValue)] = nil
        self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kDosageUUIDString.rawValue)] = nil
        self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleHourUUIDString.rawValue)] = nil
        self.characteristicsDict[CBUUID.init(string: PillPopperCharacteristic.uuidStrings.kScheduleDayUUIDString.rawValue)] = nil
    }
    
    override func viewDidLoad() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.bleActivityIndicator.startAnimating()
        centralManager = CBCentralManager(delegate: self, queue: nil)
        initializeCharacteristicsDict()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if (central.state == CBManagerState.poweredOn) {
            central.scanForPeripherals(withServices: nil, options: nil)
        } else {
            let alertController = UIAlertController(title: "PillPopper", message: "BLE not available", preferredStyle: UIAlertControllerStyle.alert)
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (_) in
                self.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let deviceName = advertisementData[CBAdvertisementDataLocalNameKey] as? String
        if(deviceName?.range(of: "PillPopperDevice") != nil) {
            self.bleStatusLabel.text = "Connecting to Device"
            self.bleStatusLabel.sizeToFit()
            central.stopScan()
            self.peripheral = peripheral
            self.peripheral?.delegate = self
            central.connect(peripheral, options: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.discoverServices([CBUUID.init(string: PillPopperCharacteristic.kServiceUUIDString)])
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services!
        {
            if(service.uuid.uuidString == PillPopperCharacteristic.kServiceUUIDString)
            {
                self.bleStatusLabel.text = "Interrogating Device"
                self.bleStatusLabel.sizeToFit()
                let keys = [CBUUID](self.characteristicsDict.keys)
                peripheral.discoverCharacteristics(keys, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        for characteristic in service.characteristics!
        {
            self.characteristicsDict[characteristic.uuid] = PillPopperCharacteristic(characteristic: characteristic)
        }
        
        let values = [PillPopperCharacteristic](self.characteristicsDict.values)
        let filteredValues = values.filter { (c) -> Bool in
            c.characteristic != nil
        }
        print(filteredValues.count)
        if(filteredValues.count >= kNumTotalCharacteristics) {
            self.performSegue(withIdentifier: "bleSuccessfulSegue", sender: self)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if(!self.disconnecting) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "bleSuccessfulSegue") {
            //populate the fields of destination view cotroller
            let dest = segue.destination as! PharmacistOptionsTableViewController
            dest.characteristicsDict = characteristicsDict
            dest.peripheral = peripheral
            dest.centralManager = centralManager
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
